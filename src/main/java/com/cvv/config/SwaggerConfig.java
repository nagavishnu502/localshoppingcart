package com.cvv.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/*@SuppressWarnings("deprecation")
@Configuration
@EnableSwagger2
@ComponentScan("com.cvv.controller.*")*/
@Configuration
public class SwaggerConfig implements WebMvcConfigurer{
	/*@Autowired
    private TypeResolver typeResolver;

    @Bean
    public Docket swaggerSpringMvcPlugin() {
    	return new Docket(DocumentationType.SWAGGER_2)  
    	          .select()                                  
    	          .apis(RequestHandlerSelectors.any())              
    	          .paths(PathSelectors.any())                          
    	          .build();
    }
*/
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){

        registry.addResourceHandler("/ui/**").addResourceLocations("classpath:/META-INF/resources/ui/");
    }

   
}