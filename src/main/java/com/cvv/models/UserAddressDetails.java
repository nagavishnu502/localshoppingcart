package com.cvv.models;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cvv_user_address_dtls")
public class UserAddressDetails {
	@Id
	@Column(name="ADDRESS_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	BigDecimal addressId;
	@Column(name="USER_ID")
	BigDecimal userId;
	@Column(name="ADDR_LANE_1")
	String usrAddrLane1;
	@Column(name="ADDR_LANE_2")
	String usrAddrLane2;
	@Column(name="VILLAGE")
	String usrVillage;
	@Column(name="MANDAL")
	String usrMandal;
	@Column(name="DISTRICT")
	String usrDistrict;
	@Column(name="CITY")
	String usrCity;
	@Column(name="STATE")
	String usrState;
	@Column(name="COUNTRY")
	String usrCountry;
	@Column(name="PINCODE")
	String usrPincode;
	@Column(name="LOGITUDE")
	String usrLogitude;
	@Column(name="LATITUDE")
	String usrLatitude;
	@Column(name="STATUS")
	String status;
	@Column(name="ACTIVEYN")
	String activeYN;
	@Column(name="CREATED_BY")
	String createdby;
	@Column(name="CREATED_DT")
	Date createdDt;
	@Column(name="LAST_MODIFIED_BY")
	String lastModifiedBy;
	@Column(name="LAST_MODIFIED_DT")
	Date lastModifiedDt;
	
	/**
	 * @return the addressId
	 */
	public BigDecimal getAddressId() {
		return addressId;
	}
	/**
	 * @param addressId the addressId to set
	 */
	public void setAddressId(BigDecimal addressId) {
		this.addressId = addressId;
	}
	/**
	 * @return the userId
	 */
	public BigDecimal getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}
	/**
	 * @return the usrAddrLane1
	 */
	public String getUsrAddrLane1() {
		return usrAddrLane1;
	}
	/**
	 * @param usrAddrLane1 the usrAddrLane1 to set
	 */
	public void setUsrAddrLane1(String usrAddrLane1) {
		this.usrAddrLane1 = usrAddrLane1;
	}
	/**
	 * @return the usrAddrLane2
	 */
	public String getUsrAddrLane2() {
		return usrAddrLane2;
	}
	/**
	 * @param usrAddrLane2 the usrAddrLane2 to set
	 */
	public void setUsrAddrLane2(String usrAddrLane2) {
		this.usrAddrLane2 = usrAddrLane2;
	}
	/**
	 * @return the usrVillage
	 */
	public String getUsrVillage() {
		return usrVillage;
	}
	/**
	 * @param usrVillage the usrVillage to set
	 */
	public void setUsrVillage(String usrVillage) {
		this.usrVillage = usrVillage;
	}
	/**
	 * @return the usrMandal
	 */
	public String getUsrMandal() {
		return usrMandal;
	}
	/**
	 * @param usrMandal the usrMandal to set
	 */
	public void setUsrMandal(String usrMandal) {
		this.usrMandal = usrMandal;
	}
	/**
	 * @return the usrDistrict
	 */
	public String getUsrDistrict() {
		return usrDistrict;
	}
	/**
	 * @param usrDistrict the usrDistrict to set
	 */
	public void setUsrDistrict(String usrDistrict) {
		this.usrDistrict = usrDistrict;
	}
	/**
	 * @return the usrCity
	 */
	public String getUsrCity() {
		return usrCity;
	}
	/**
	 * @param usrCity the usrCity to set
	 */
	public void setUsrCity(String usrCity) {
		this.usrCity = usrCity;
	}
	/**
	 * @return the usrState
	 */
	public String getUsrState() {
		return usrState;
	}
	/**
	 * @param usrState the usrState to set
	 */
	public void setUsrState(String usrState) {
		this.usrState = usrState;
	}
	/**
	 * @return the usrCountry
	 */
	public String getUsrCountry() {
		return usrCountry;
	}
	/**
	 * @param usrCountry the usrCountry to set
	 */
	public void setUsrCountry(String usrCountry) {
		this.usrCountry = usrCountry;
	}
	/**
	 * @return the usrPincode
	 */
	public String getUsrPincode() {
		return usrPincode;
	}
	/**
	 * @param usrPincode the usrPincode to set
	 */
	public void setUsrPincode(String usrPincode) {
		this.usrPincode = usrPincode;
	}
	/**
	 * @return the usrLogitude
	 */
	public String getUsrLogitude() {
		return usrLogitude;
	}
	/**
	 * @param usrLogitude the usrLogitude to set
	 */
	public void setUsrLogitude(String usrLogitude) {
		this.usrLogitude = usrLogitude;
	}
	/**
	 * @return the usrLatitude
	 */
	public String getUsrLatitude() {
		return usrLatitude;
	}
	/**
	 * @param usrLatitude the usrLatitude to set
	 */
	public void setUsrLatitude(String usrLatitude) {
		this.usrLatitude = usrLatitude;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the activeYN
	 */
	public String getActiveYN() {
		return activeYN;
	}
	/**
	 * @param activeYN the activeYN to set
	 */
	public void setActiveYN(String activeYN) {
		this.activeYN = activeYN;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	/**
	 * @param lastModifiedBy the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	/**
	 * @return the lastModifiedDt
	 */
	public Date getLastModifiedDt() {
		return lastModifiedDt;
	}
	/**
	 * @param lastModifiedDt the lastModifiedDt to set
	 */
	public void setLastModifiedDt(Date lastModifiedDt) {
		this.lastModifiedDt = lastModifiedDt;
	}
	
	
}
