package com.cvv.models;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="cvv_meta_product_dtls")
public class MetaProductDtls {
	@Id
	@Column(name="PRODUCT_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	/*@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "meta_prdt_dtls_Sequence")
	@SequenceGenerator(name = "meta_prdt_dtls_Sequence", sequenceName = "cvv_meta_prdt_dtls_seq",allocationSize = 1)*/
	BigDecimal productId;
	@Column(name="USER_ID")
	BigDecimal userId;
	@Column(name="SHOP_ID")
	BigDecimal shopId;
	@Column(name="SHP_GROUP_ID")
	BigDecimal shpGrpId;
	@Column(name="PRODUCT_NO")
	String prodNo;
	@Column(name="PRODUCT_NAME")
	String prodName;
	@Column(name="PRODUCT_DESC")
	String prodDesc;
	@Column(name="PRODUCT_PIC")
	String prodPic;	
	@Column(name="DEFAULT_PRICE")
	Float defaultPrice;
	@Column(name="MEASURE_UNIT")
	String measureUnit;
	@Column(name="DISCOUNT_PERC")
	String discountPerc;
	@Column(name="DISCOUNT_PRICE")
	Float discountPrice;
	@Column(name="STATUS")
	String status;
	@Column(name="ACTIVEYN")
	String activeYN;
	@Column(name="CREATED_BY")
	String createdBy;
	@Column(name="CREATED_DT")
	Date createdDt;
	@Column(name="LAST_MODIFIED_BY")
	String lastModifiedBy;
	@Column(name="LAST_MODIFIED_DT")
	Date lastModifiedDt;
	/**
	 * @return the productId
	 */
	public BigDecimal getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(BigDecimal productId) {
		this.productId = productId;
	}
	/**
	 * @return the userId
	 */
	public BigDecimal getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}
	/**
	 * @return the shopId
	 */
	public BigDecimal getShopId() {
		return shopId;
	}
	/**
	 * @param shopId the shopId to set
	 */
	public void setShopId(BigDecimal shopId) {
		this.shopId = shopId;
	}
	/**
	 * @return the shpGrpId
	 */
	public BigDecimal getShpGrpId() {
		return shpGrpId;
	}
	/**
	 * @param shpGrpId the shpGrpId to set
	 */
	public void setShpGrpId(BigDecimal shpGrpId) {
		this.shpGrpId = shpGrpId;
	}
	/**
	 * @return the prodNo
	 */
	public String getProdNo() {
		return prodNo;
	}
	/**
	 * @param prodNo the prodNo to set
	 */
	public void setProdNo(String prodNo) {
		this.prodNo = prodNo;
	}
	/**
	 * @return the prodName
	 */
	public String getProdName() {
		return prodName;
	}
	/**
	 * @param prodName the prodName to set
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	/**
	 * @return the prodDesc
	 */
	public String getProdDesc() {
		return prodDesc;
	}
	/**
	 * @param prodDesc the prodDesc to set
	 */
	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}
	/**
	 * @return the prodPic
	 */
	public String getProdPic() {
		return prodPic;
	}
	/**
	 * @param prodPic the prodPic to set
	 */
	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}
	/**
	 * @return the defaultPrice
	 */
	public Float getDefaultPrice() {
		return defaultPrice;
	}
	/**
	 * @param defaultPrice the defaultPrice to set
	 */
	public void setDefaultPrice(Float defaultPrice) {
		this.defaultPrice = defaultPrice;
	}
	/**
	 * @return the measureUnit
	 */
	public String getMeasureUnit() {
		return measureUnit;
	}
	/**
	 * @param measureUnit the measureUnit to set
	 */
	public void setMeasureUnit(String measureUnit) {
		this.measureUnit = measureUnit;
	}
	/**
	 * @return the discountPerc
	 */
	public String getDiscountPerc() {
		return discountPerc;
	}
	/**
	 * @param discountPerc the discountPerc to set
	 */
	public void setDiscountPerc(String discountPerc) {
		this.discountPerc = discountPerc;
	}
	/**
	 * @return the discountPrice
	 */
	public Float getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * @param discountPrice the discountPrice to set
	 */
	public void setDiscountPrice(Float discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the activeYN
	 */
	public String getActiveYN() {
		return activeYN;
	}
	/**
	 * @param activeYN the activeYN to set
	 */
	public void setActiveYN(String activeYN) {
		this.activeYN = activeYN;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	/**
	 * @param lastModifiedBy the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	/**
	 * @return the lastModifiedDt
	 */
	public Date getLastModifiedDt() {
		return lastModifiedDt;
	}
	/**
	 * @param lastModifiedDt the lastModifiedDt to set
	 */
	public void setLastModifiedDt(Date lastModifiedDt) {
		this.lastModifiedDt = lastModifiedDt;
	}
	
}
