package com.cvv.models;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="cvv_meta_addon_dtls")
public class MetaAddonDtls {
	@Id
	@Column(name="addon_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	/*@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "meta_prdt_dtls_Sequence")
	@SequenceGenerator(name = "meta_prdt_dtls_Sequence", sequenceName = "cvv_meta_prdt_dtls_seq",allocationSize = 1)*/
	BigDecimal addonId;
	@Column(name="USER_ID")
	BigDecimal userId;
	@Column(name="addon_type")
	String addonType;
	@Column(name="addon_value")
	String addonValue;
	@Column(name="addon_desc")
	String addonDesc;
	@Column(name="addon_pic")
	String addonPic;
	@Column(name="STATUS")
	String status;
	@Column(name="ACTIVEYN")
	String activeYN;
	@Column(name="CREATED_BY")
	String createdBy;
	@Column(name="CREATED_DT")
	Date createdDt;
	@Column(name="LAST_MODIFIED_BY")
	String lastModifiedBy;
	@Column(name="LAST_MODIFIED_DT")
	Date lastModifiedDt;
	/**
	 * @return the addonId
	 */
	public BigDecimal getAddonId() {
		return addonId;
	}
	/**
	 * @param addonId the addonId to set
	 */
	public void setAddonId(BigDecimal addonId) {
		this.addonId = addonId;
	}
	/**
	 * @return the userId
	 */
	public BigDecimal getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}
	/**
	 * @return the addonType
	 */
	public String getAddonType() {
		return addonType;
	}
	/**
	 * @param addonType the addonType to set
	 */
	public void setAddonType(String addonType) {
		this.addonType = addonType;
	}
	/**
	 * @return the addonValue
	 */
	public String getAddonValue() {
		return addonValue;
	}
	/**
	 * @param addonValue the addonValue to set
	 */
	public void setAddonValue(String addonValue) {
		this.addonValue = addonValue;
	}
	/**
	 * @return the addonDesc
	 */
	public String getAddonDesc() {
		return addonDesc;
	}
	/**
	 * @param addonDesc the addonDesc to set
	 */
	public void setAddonDesc(String addonDesc) {
		this.addonDesc = addonDesc;
	}
	/**
	 * @return the addonPic
	 */
	public String getAddonPic() {
		return addonPic;
	}
	/**
	 * @param addonPic the addonPic to set
	 */
	public void setAddonPic(String addonPic) {
		this.addonPic = addonPic;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the activeYN
	 */
	public String getActiveYN() {
		return activeYN;
	}
	/**
	 * @param activeYN the activeYN to set
	 */
	public void setActiveYN(String activeYN) {
		this.activeYN = activeYN;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	/**
	 * @param lastModifiedBy the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	/**
	 * @return the lastModifiedDt
	 */
	public Date getLastModifiedDt() {
		return lastModifiedDt;
	}
	/**
	 * @param lastModifiedDt the lastModifiedDt to set
	 */
	public void setLastModifiedDt(Date lastModifiedDt) {
		this.lastModifiedDt = lastModifiedDt;
	}
	
}
