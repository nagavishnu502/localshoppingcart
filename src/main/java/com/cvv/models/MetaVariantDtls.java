package com.cvv.models;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="cvv_meta_variant_dtls")
public class MetaVariantDtls {
	@Id
	@Column(name="variant_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	/*@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "meta_prdt_dtls_Sequence")
	@SequenceGenerator(name = "meta_prdt_dtls_Sequence", sequenceName = "cvv_meta_prdt_dtls_seq",allocationSize = 1)*/
	BigDecimal variantId;
	@Column(name="USER_ID")
	BigDecimal userId;
	@Column(name="variant_type")
	String variantType;
	@Column(name="variant_value")
	String variantValue;
	@Column(name="variant_desc")
	String variantDesc;
	@Column(name="STATUS")
	String status;
	@Column(name="ACTIVEYN")
	String activeYN;
	@Column(name="CREATED_BY")
	String createdBy;
	@Column(name="CREATED_DT")
	Date createdDt;
	@Column(name="LAST_MODIFIED_BY")
	String lastModifiedBy;
	@Column(name="LAST_MODIFIED_DT")
	Date lastModifiedDt;
	/**
	 * @return the variantId
	 */
	public BigDecimal getVariantId() {
		return variantId;
	}
	/**
	 * @param variantId the variantId to set
	 */
	public void setVariantId(BigDecimal variantId) {
		this.variantId = variantId;
	}
	/**
	 * @return the userId
	 */
	public BigDecimal getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}
	/**
	 * @return the variantType
	 */
	public String getVariantType() {
		return variantType;
	}
	/**
	 * @param variantType the variantType to set
	 */
	public void setVariantType(String variantType) {
		this.variantType = variantType;
	}
	/**
	 * @return the variantValue
	 */
	public String getVariantValue() {
		return variantValue;
	}
	/**
	 * @param variantValue the variantValue to set
	 */
	public void setVariantValue(String variantValue) {
		this.variantValue = variantValue;
	}
	/**
	 * @return the variantDesc
	 */
	public String getVariantDesc() {
		return variantDesc;
	}
	/**
	 * @param variantDesc the variantDesc to set
	 */
	public void setVariantDesc(String variantDesc) {
		this.variantDesc = variantDesc;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the activeYN
	 */
	public String getActiveYN() {
		return activeYN;
	}
	/**
	 * @param activeYN the activeYN to set
	 */
	public void setActiveYN(String activeYN) {
		this.activeYN = activeYN;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	/**
	 * @param lastModifiedBy the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	/**
	 * @return the lastModifiedDt
	 */
	public Date getLastModifiedDt() {
		return lastModifiedDt;
	}
	/**
	 * @param lastModifiedDt the lastModifiedDt to set
	 */
	public void setLastModifiedDt(Date lastModifiedDt) {
		this.lastModifiedDt = lastModifiedDt;
	}
	
}
