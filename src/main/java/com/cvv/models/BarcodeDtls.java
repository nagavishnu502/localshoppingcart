package com.cvv.models;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="cvv_barcode_dtls")
public class BarcodeDtls {
	@Id
	@Column(name="barcode_number")	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	/*@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "shop_prdt_dtls_Sequence")
	@SequenceGenerator(name = "shop_prdt_dtls_Sequence", sequenceName = "cvv_shop_prdt_dtls_seq",allocationSize = 1)*/
	BigDecimal barcodeNumber;
	@Column(name="barcode_formats")
	String barcodeFormat;
	@Column(name="mpn")
	String mpn;
	@Column(name="model")
	String model;
	@Column(name="asin")
	String asin;
	@Column(name="title")
	String title;
	@Column(name="category")
	String category;
	@Column(name="manufacturer")
	String manufacturer;
	@Column(name="brand")
	String brand;
	@Column(name="age_group")
	String age_group;
	@Column(name="ingredients")
	String ingredients;
	@Column(name="nutrition_facts")
	String nutrition_facts;
	@Column(name="energy_efficiency_class")
	String energy_efficiency_class;
	@Column(name="color")
	String color;
	@Column(name="gender")
	String gender;
	@Column(name="material")
	String material;
	@Column(name="pattern")
	String pattern;
	@Column(name="format")
	String format;
	@Column(name="multipack")
	String multipack;
	@Column(name="size")
	String size;
	@Column(name="length")
	String length;
	@Column(name="width")
	String width;
	@Column(name="height")
	String height;
	@Column(name="weight")
	String weight;
	@Column(name="release_date")
	String release_date;
	@Column(name="description")
	String description;	
	@Column(name="images")
	String images;
	/**
	 * @return the barcodeNumber
	 */
	public BigDecimal getBarcodeNumber() {
		return barcodeNumber;
	}
	/**
	 * @param barcodeNumber the barcodeNumber to set
	 */
	public void setBarcodeNumber(BigDecimal barcodeNumber) {
		this.barcodeNumber = barcodeNumber;
	}
	/**
	 * @return the barcodeFormat
	 */
	public String getBarcodeFormat() {
		return barcodeFormat;
	}
	/**
	 * @param barcodeFormat the barcodeFormat to set
	 */
	public void setBarcodeFormat(String barcodeFormat) {
		this.barcodeFormat = barcodeFormat;
	}
	/**
	 * @return the mpn
	 */
	public String getMpn() {
		return mpn;
	}
	/**
	 * @param mpn the mpn to set
	 */
	public void setMpn(String mpn) {
		this.mpn = mpn;
	}
	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}
	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}
	/**
	 * @return the asin
	 */
	public String getAsin() {
		return asin;
	}
	/**
	 * @param asin the asin to set
	 */
	public void setAsin(String asin) {
		this.asin = asin;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the manufacturer
	 */
	public String getManufacturer() {
		return manufacturer;
	}
	/**
	 * @param manufacturer the manufacturer to set
	 */
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}
	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}
	/**
	 * @return the age_group
	 */
	public String getAge_group() {
		return age_group;
	}
	/**
	 * @param age_group the age_group to set
	 */
	public void setAge_group(String age_group) {
		this.age_group = age_group;
	}
	/**
	 * @return the ingredients
	 */
	public String getIngredients() {
		return ingredients;
	}
	/**
	 * @param ingredients the ingredients to set
	 */
	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}
	/**
	 * @return the nutrition_facts
	 */
	public String getNutrition_facts() {
		return nutrition_facts;
	}
	/**
	 * @param nutrition_facts the nutrition_facts to set
	 */
	public void setNutrition_facts(String nutrition_facts) {
		this.nutrition_facts = nutrition_facts;
	}
	/**
	 * @return the energy_efficiency_class
	 */
	public String getEnergy_efficiency_class() {
		return energy_efficiency_class;
	}
	/**
	 * @param energy_efficiency_class the energy_efficiency_class to set
	 */
	public void setEnergy_efficiency_class(String energy_efficiency_class) {
		this.energy_efficiency_class = energy_efficiency_class;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the material
	 */
	public String getMaterial() {
		return material;
	}
	/**
	 * @param material the material to set
	 */
	public void setMaterial(String material) {
		this.material = material;
	}
	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern;
	}
	/**
	 * @param pattern the pattern to set
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}
	/**
	 * @param format the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}
	/**
	 * @return the multipack
	 */
	public String getMultipack() {
		return multipack;
	}
	/**
	 * @param multipack the multipack to set
	 */
	public void setMultipack(String multipack) {
		this.multipack = multipack;
	}
	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}
	/**
	 * @return the length
	 */
	public String getLength() {
		return length;
	}
	/**
	 * @param length the length to set
	 */
	public void setLength(String length) {
		this.length = length;
	}
	/**
	 * @return the width
	 */
	public String getWidth() {
		return width;
	}
	/**
	 * @param width the width to set
	 */
	public void setWidth(String width) {
		this.width = width;
	}
	/**
	 * @return the height
	 */
	public String getHeight() {
		return height;
	}
	/**
	 * @param height the height to set
	 */
	public void setHeight(String height) {
		this.height = height;
	}
	/**
	 * @return the weight
	 */
	public String getWeight() {
		return weight;
	}
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(String weight) {
		this.weight = weight;
	}
	/**
	 * @return the release_date
	 */
	public String getRelease_date() {
		return release_date;
	}
	/**
	 * @param release_date the release_date to set
	 */
	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the images
	 */
	public String getImages() {
		return images;
	}
	/**
	 * @param images the images to set
	 */
	public void setImages(String images) {
		this.images = images;
	}
	
}
