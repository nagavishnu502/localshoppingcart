package com.cvv.models;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cvv_prdt_variant_dtls")
public class ProdVariantDtls {
	@Id
	@Column(name="variant_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	BigDecimal variantId;
	@Column(name="variant_name")
	String variantName;
	@Column(name="variant_pic")
	String variantPic;
	@Column(name="variant_type")
	String variantType;
	@Column(name="variant_prdt_price")
	Float variantPrdtPrice;
	@Column(name="discount_price")
	Float discountPrice;
	@Column(name="discount_perc")
	String discountPerc;
	@Column(name="product_id")
	BigDecimal productId;
	@Column(name="STATUS")
	String status;
	@Column(name="ACTIVEYN")
	String activeYN;
	@Column(name="CREATED_BY")
	String createdBy;
	@Column(name="CREATED_DT")
	Date createdDt;
	@Column(name="LAST_MODIFIED_BY")
	String lastModifiedBy;
	@Column(name="LAST_MODIFIED_DT")
	Date lastModifiedDt;
	/**
	 * @return the variantId
	 */
	public BigDecimal getVariantId() {
		return variantId;
	}
	/**
	 * @param variantId the variantId to set
	 */
	public void setVariantId(BigDecimal variantId) {
		this.variantId = variantId;
	}
	/**
	 * @return the variantName
	 */
	public String getVariantName() {
		return variantName;
	}
	/**
	 * @param variantName the variantName to set
	 */
	public void setVariantName(String variantName) {
		this.variantName = variantName;
	}
	/**
	 * @return the variantPic
	 */
	public String getVariantPic() {
		return variantPic;
	}
	/**
	 * @param variantPic the variantPic to set
	 */
	public void setVariantPic(String variantPic) {
		this.variantPic = variantPic;
	}
	/**
	 * @return the variantType
	 */
	public String getVariantType() {
		return variantType;
	}
	/**
	 * @param variantType the variantType to set
	 */
	public void setVariantType(String variantType) {
		this.variantType = variantType;
	}
	/**
	 * @return the variantPrdtPrice
	 */
	public Float getVariantPrdtPrice() {
		return variantPrdtPrice;
	}
	/**
	 * @param variantPrdtPrice the variantPrdtPrice to set
	 */
	public void setVariantPrdtPrice(Float variantPrdtPrice) {
		this.variantPrdtPrice = variantPrdtPrice;
	}
	/**
	 * @return the discountPrice
	 */
	public Float getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * @param discountPrice the discountPrice to set
	 */
	public void setDiscountPrice(Float discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * @return the discountPerc
	 */
	public String getDiscountPerc() {
		return discountPerc;
	}
	/**
	 * @param discountPerc the discountPerc to set
	 */
	public void setDiscountPerc(String discountPerc) {
		this.discountPerc = discountPerc;
	}
	/**
	 * @return the productId
	 */
	public BigDecimal getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(BigDecimal productId) {
		this.productId = productId;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the activeYN
	 */
	public String getActiveYN() {
		return activeYN;
	}
	/**
	 * @param activeYN the activeYN to set
	 */
	public void setActiveYN(String activeYN) {
		this.activeYN = activeYN;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	/**
	 * @param lastModifiedBy the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	/**
	 * @return the lastModifiedDt
	 */
	public Date getLastModifiedDt() {
		return lastModifiedDt;
	}
	/**
	 * @param lastModifiedDt the lastModifiedDt to set
	 */
	public void setLastModifiedDt(Date lastModifiedDt) {
		this.lastModifiedDt = lastModifiedDt;
	}
	
}
