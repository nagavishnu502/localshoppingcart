package com.cvv.models;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cvv_prdt_addon_dtls")
public class ProdAddonDtls {
	@Id
	@Column(name="addon_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	BigDecimal addonId;
	@Column(name="addon_name")
	String addonName;
	@Column(name="addon_pic")
	String addonPic;
	@Column(name="product_id")
	BigDecimal productId;
	@Column(name="addon_prdt_price")
	Float addonPrdtPrice;
	@Column(name="discount_perc")
	String discountPerc;
	@Column(name="STATUS")
	String status;
	@Column(name="ACTIVEYN")
	String activeYN;
	@Column(name="CREATED_BY")
	String createdBy;
	@Column(name="CREATED_DT")
	Date createdDt;
	@Column(name="LAST_MODIFIED_BY")
	String lastModifiedBy;
	@Column(name="LAST_MODIFIED_DT")
	Date lastModifiedDt;
	/**
	 * @return the addonId
	 */
	public BigDecimal getAddonId() {
		return addonId;
	}
	/**
	 * @param addonId the addonId to set
	 */
	public void setAddonId(BigDecimal addonId) {
		this.addonId = addonId;
	}
	/**
	 * @return the addonName
	 */
	public String getAddonName() {
		return addonName;
	}
	/**
	 * @param addonName the addonName to set
	 */
	public void setAddonName(String addonName) {
		this.addonName = addonName;
	}
	/**
	 * @return the addonPic
	 */
	public String getAddonPic() {
		return addonPic;
	}
	/**
	 * @param addonPic the addonPic to set
	 */
	public void setAddonPic(String addonPic) {
		this.addonPic = addonPic;
	}
	/**
	 * @return the productId
	 */
	public BigDecimal getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(BigDecimal productId) {
		this.productId = productId;
	}
	/**
	 * @return the addonPrdtPrice
	 */
	public Float getAddonPrdtPrice() {
		return addonPrdtPrice;
	}
	/**
	 * @param addonPrdtPrice the addonPrdtPrice to set
	 */
	public void setAddonPrdtPrice(Float addonPrdtPrice) {
		this.addonPrdtPrice = addonPrdtPrice;
	}
	/**
	 * @return the discountPerc
	 */
	public String getDiscountPerc() {
		return discountPerc;
	}
	/**
	 * @param discountPerc the discountPerc to set
	 */
	public void setDiscountPerc(String discountPerc) {
		this.discountPerc = discountPerc;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the activeYN
	 */
	public String getActiveYN() {
		return activeYN;
	}
	/**
	 * @param activeYN the activeYN to set
	 */
	public void setActiveYN(String activeYN) {
		this.activeYN = activeYN;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	/**
	 * @param lastModifiedBy the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	/**
	 * @return the lastModifiedDt
	 */
	public Date getLastModifiedDt() {
		return lastModifiedDt;
	}
	/**
	 * @param lastModifiedDt the lastModifiedDt to set
	 */
	public void setLastModifiedDt(Date lastModifiedDt) {
		this.lastModifiedDt = lastModifiedDt;
	}
	
}
