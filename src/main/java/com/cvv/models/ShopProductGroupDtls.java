package com.cvv.models;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="cvv_usr_shp_prdt_grp")
public class ShopProductGroupDtls {
	@Id
	@Column(name="SHP_GROUP_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	/*@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "shop_prdt_grp_Sequence")
	@SequenceGenerator(name = "shop_prdt_grp_Sequence", sequenceName = "cvv_shop_prdt_grp_seq",allocationSize = 1)*/
	BigDecimal shopGrpId;
	@Column(name="USER_ID")
	BigDecimal userId;
	@Column(name="SHOP_ID")
	BigDecimal shopId;
	@Column(name="GROUP_NAME")
	String groupName;
	@Column(name="GROUP_DESC")
	String groupDesc;
	@Column(name="GROUP_PIC")
	String groupPic;
	@Column(name="PARENT_GRP_ID")
	BigDecimal parentGrpId;
	@Column(name="STATUS")
	String status;
	@Column(name="ACTIVEYN")
	String activeYN;
	@Column(name="CREATED_BY")
	String createdBy;
	@Column(name="CREATED_DT")
	Date createdDt;
	@Column(name="LAST_MODIFIED_BY")
	String lastModifiedBy;
	@Column(name="LAST_MODIFIED_DT")
	Date lastModifiedDt;
	/**
	 * @return the shopGrpId
	 */
	public BigDecimal getShopGrpId() {
		return shopGrpId;
	}
	/**
	 * @param shopGrpId the shopGrpId to set
	 */
	public void setShopGrpId(BigDecimal shopGrpId) {
		this.shopGrpId = shopGrpId;
	}
	/**
	 * @return the userId
	 */
	public BigDecimal getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}
	/**
	 * @return the shopId
	 */
	public BigDecimal getShopId() {
		return shopId;
	}
	/**
	 * @param shopId the shopId to set
	 */
	public void setShopId(BigDecimal shopId) {
		this.shopId = shopId;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @return the groupDesc
	 */
	public String getGroupDesc() {
		return groupDesc;
	}
	/**
	 * @param groupDesc the groupDesc to set
	 */
	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}
	/**
	 * @return the groupPic
	 */
	public String getGroupPic() {
		return groupPic;
	}
	/**
	 * @param groupPic the groupPic to set
	 */
	public void setGroupPic(String groupPic) {
		this.groupPic = groupPic;
	}
	/**
	 * @return the parentGrpId
	 */
	public BigDecimal getParentGrpId() {
		return parentGrpId;
	}
	/**
	 * @param parentGrpId the parentGrpId to set
	 */
	public void setParentGrpId(BigDecimal parentGrpId) {
		this.parentGrpId = parentGrpId;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the activeYN
	 */
	public String getActiveYN() {
		return activeYN;
	}
	/**
	 * @param activeYN the activeYN to set
	 */
	public void setActiveYN(String activeYN) {
		this.activeYN = activeYN;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	/**
	 * @param lastModifiedBy the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	/**
	 * @return the lastModifiedDt
	 */
	public Date getLastModifiedDt() {
		return lastModifiedDt;
	}
	/**
	 * @param lastModifiedDt the lastModifiedDt to set
	 */
	public void setLastModifiedDt(Date lastModifiedDt) {
		this.lastModifiedDt = lastModifiedDt;
	}
	
}
