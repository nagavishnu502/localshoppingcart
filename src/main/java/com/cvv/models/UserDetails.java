package com.cvv.models;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="cvv_user")
public class UserDetails {
	@Id
	@Column(name="USER_ID")	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	/*@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "user_Sequence")
	@SequenceGenerator(name = "user_Sequence", sequenceName = "cvv_user_seq",allocationSize = 1)*/
	BigDecimal usrId; 
	@Column(name="USER_FNAME")
	String usrFirstName;
	@Column(name="USER_LNAME")
	String usrLastName;
	@Column(name="USER_NAME")
	String usrFullName;
	@Column(name="USER_EMAILID")
	String usrEmailId;	
	@Column(name="USER_PROFILE_PIC")
	String usrProfilePic;
	@Column(name="USER_REFFERAL_CODE")
	String usrRefferalCode;
	@Column(name="USER_REFFERED_BY")
	String usrRefferedBy;
	@Column(name="STATUS")
	String status;
	@Column(name="ACTIVEYN")
	String activeYN;
	@Column(name="CREATED_BY")
	String createdBy;
	@Column(name="CREATED_DT")
	Date createdDt;
	@Column(name="LAST_MODIFIED_BY")
	String lastModifiedBy;
	@Column(name="LAST_MODIFIED_DT")
	String lastModifiedDt;
	/*List<UserRoleDetails> usrRoleList;
	List<UserContactDetails> usrContactList;
	List<UserAddressDetails> usrAddressList;
	List<ShopDetails> shopList;*/
	/**
	 * @return the usrId
	 */
	public BigDecimal getUsrId() {
		return usrId;
	}
	/**
	 * @param usrId the usrId to set
	 */
	public void setUsrId(BigDecimal usrId) {
		this.usrId = usrId;
	}
	/**
	 * @return the usrFirstName
	 */
	public String getUsrFirstName() {
		return usrFirstName;
	}
	/**
	 * @param usrFirstName the usrFirstName to set
	 */
	public void setUsrFirstName(String usrFirstName) {
		this.usrFirstName = usrFirstName;
	}
	/**
	 * @return the usrLastName
	 */
	public String getUsrLastName() {
		return usrLastName;
	}
	/**
	 * @param usrLastName the usrLastName to set
	 */
	public void setUsrLastName(String usrLastName) {
		this.usrLastName = usrLastName;
	}
	/**
	 * @return the usrFullName
	 */
	public String getUsrFullName() {
		return usrFullName;
	}
	/**
	 * @param usrFullName the usrFullName to set
	 */
	public void setUsrFullName(String usrFullName) {
		this.usrFullName = usrFullName;
	}
	/**
	 * @return the usrProfilePic
	 */
	public String getUsrProfilePic() {
		return usrProfilePic;
	}
	/**
	 * @param usrProfilePic the usrProfilePic to set
	 */
	public void setUsrProfilePic(String usrProfilePic) {
		this.usrProfilePic = usrProfilePic;
	}
	/**
	 * @return the usrRefferalCode
	 */
	public String getUsrRefferalCode() {
		return usrRefferalCode;
	}
	/**
	 * @param usrRefferalCode the usrRefferalCode to set
	 */
	public void setUsrRefferalCode(String usrRefferalCode) {
		this.usrRefferalCode = usrRefferalCode;
	}
	/**
	 * @return the usrRefferedBy
	 */
	public String getUsrRefferedBy() {
		return usrRefferedBy;
	}
	/**
	 * @param usrRefferedBy the usrRefferedBy to set
	 */
	public void setUsrRefferedBy(String usrRefferedBy) {
		this.usrRefferedBy = usrRefferedBy;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the activeYN
	 */
	public String getActiveYN() {
		return activeYN;
	}
	/**
	 * @param activeYN the activeYN to set
	 */
	public void setActiveYN(String activeYN) {
		this.activeYN = activeYN;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	/**
	 * @param lastModifiedBy the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	/**
	 * @return the lastModifiedDt
	 */
	public String getLastModifiedDt() {
		return lastModifiedDt;
	}
	/**
	 * @param lastModifiedDt the lastModifiedDt to set
	 */
	public void setLastModifiedDt(String lastModifiedDt) {
		this.lastModifiedDt = lastModifiedDt;
	}
	/**
	 * @return the usrEmailId
	 */
	public String getUsrEmailId() {
		return usrEmailId;
	}
	/**
	 * @param usrEmailId the usrEmailId to set
	 */
	public void setUsrEmailId(String usrEmailId) {
		this.usrEmailId = usrEmailId;
	}
	
	
}
