package com.cvv.models;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="cvv_user_shop_dtls")
public class ShopDetails {
	@Id
	@Column(name="SHOP_ID")	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	/*@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "user_shop_dtls_Sequence")
	@SequenceGenerator(name = "user_shop_dtls_Sequence", sequenceName = "cvv_user_shop_dtls_seq",allocationSize = 1)*/
	BigDecimal shopId;
	@Column(name="USER_ID")
	BigDecimal userId;
	@Column(name="SHOP_NAME")
	String shopName;
	@Column(name="SHOP_REG_NO")
	String shopRegNo;
	@Column(name="SHOP_PIC")
	String shopPic;
	@Column(name="STATUS")
	String status;
	@Column(name="ACTIVEYN")
	String activeYN;
	@Column(name="CREATED_BY")
	String createdBy;
	@Column(name="CREATED_DT")
	Date createdDt;
	@Column(name="LAST_MODIFIED_BY")
	String lastModifiedBy;
	@Column(name="LAST_MODIFIED_DT")
	Date lastModifiedDt;
	@Column(name="business_id")
	String businessId;
	/**
	 * @return the shopId
	 */
	public BigDecimal getShopId() {
		return shopId;
	}
	/**
	 * @param shopId the shopId to set
	 */
	public void setShopId(BigDecimal shopId) {
		this.shopId = shopId;
	}
	/**
	 * @return the userId
	 */
	public BigDecimal getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}
	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}
	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	/**
	 * @return the shopRegNo
	 */
	public String getShopRegNo() {
		return shopRegNo;
	}
	/**
	 * @param shopRegNo the shopRegNo to set
	 */
	public void setShopRegNo(String shopRegNo) {
		this.shopRegNo = shopRegNo;
	}
	/**
	 * @return the shopPic
	 */
	public String getShopPic() {
		return shopPic;
	}
	/**
	 * @param shopPic the shopPic to set
	 */
	public void setShopPic(String shopPic) {
		this.shopPic = shopPic;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the activeYN
	 */
	public String getActiveYN() {
		return activeYN;
	}
	/**
	 * @param activeYN the activeYN to set
	 */
	public void setActiveYN(String activeYN) {
		this.activeYN = activeYN;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	/**
	 * @param lastModifiedBy the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	/**
	 * @return the lastModifiedDt
	 */
	public Date getLastModifiedDt() {
		return lastModifiedDt;
	}
	/**
	 * @param lastModifiedDt the lastModifiedDt to set
	 */
	public void setLastModifiedDt(Date lastModifiedDt) {
		this.lastModifiedDt = lastModifiedDt;
	}
	/**
	 * @return the businessId
	 */
	public String getBusinessId() {
		return businessId;
	}
	/**
	 * @param businessId the businessId to set
	 */
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	
}
