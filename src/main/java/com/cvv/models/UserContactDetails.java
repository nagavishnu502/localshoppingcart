package com.cvv.models;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cvv_user_contact_dtls")
public class UserContactDetails {
	@Id
	@Column(name="CONTACT_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	BigDecimal contactId;
	@Column(name="USER_ID")
	BigDecimal userId;
	@Column(name="PHONE_NO")
	String usrContactNo;
	@Column(name="ALTER_PHONE_NO")
	String usrAlternateNo;
	@Column(name="EMAIL_ID")
	String usrEmail;
	@Column(name="STATUS")
	String status;
	@Column(name="ACTIVEYN")
	String activeYN;
	@Column(name="CREATED_BY")
	String createdBy;
	@Column(name="CREATED_DT")
	Date createdDt;
	@Column(name="LAST_MODIFIED_BY")
	String lastModifiedBy;
	@Column(name="LAST_MODIFIED_DT")
	Date lastModifiedDt;

	public BigDecimal getContactId() {
		return contactId;
	}

	public void setContactId(BigDecimal contactId) {
		this.contactId = contactId;
	}

	public BigDecimal getUserId() {
		return userId;
	}

	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}

	public String getUsrContactNo() {
		return usrContactNo;
	}

	public void setUsrContactNo(String usrContactNo) {
		this.usrContactNo = usrContactNo;
	}

	public String getUsrAlternateNo() {
		return usrAlternateNo;
	}

	public void setUsrAlternateNo(String usrAlternateNo) {
		this.usrAlternateNo = usrAlternateNo;
	}

	public String getUsrEmail() {
		return usrEmail;
	}

	public void setUsrEmail(String usrEmail) {
		this.usrEmail = usrEmail;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getActiveYN() {
		return activeYN;
	}

	public void setActiveYN(String activeYN) {
		this.activeYN = activeYN;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDt() {
		return lastModifiedDt;
	}

	public void setLastModifiedDt(Date lastModifiedDt) {
		this.lastModifiedDt = lastModifiedDt;
	}

}
