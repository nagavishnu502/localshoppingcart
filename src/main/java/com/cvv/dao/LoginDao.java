package com.cvv.dao;

import java.util.List;

import com.cvv.models.BusinessTypes;
import com.cvv.models.UserDetails;

public interface LoginDao {

	public boolean checkProfile(UserDetails usrDtls) throws Exception;
	public UserDetails createProfile(UserDetails usrDtls) throws Exception;
	public UserDetails getProfile(String email) throws Exception;
	public List<BusinessTypes> getBusinessTypeDtls() throws Exception;
	
} 
