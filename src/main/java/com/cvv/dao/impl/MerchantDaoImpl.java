package com.cvv.dao.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cvv.dao.MerchantDao;
import com.cvv.models.BarcodeDtls;
import com.cvv.models.MetaAddonDtls;
import com.cvv.models.MetaProductDtls;
import com.cvv.models.MetaProductGroupDtls;
import com.cvv.models.MetaVariantDtls;
import com.cvv.models.ProdAddonDtls;
import com.cvv.models.ProdVariantDtls;
import com.cvv.models.ShopDetails;
import com.cvv.models.ShopProductDtls;
import com.cvv.models.ShopProductGroupDtls;
import com.cvv.models.UserDetails;

@Repository
public class MerchantDaoImpl implements MerchantDao {
	@Autowired
	public SessionFactory sessionFac;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ShopDetails createShop(ShopDetails shopDtls) throws Exception {
		try {
			shopDtls.setActiveYN("Y");
			shopDtls.setCreatedBy("SYSTEM");
			shopDtls.setCreatedDt(new Date(new java.util.Date().getTime()));
			shopDtls = (ShopDetails) sessionFac.getCurrentSession().merge(shopDtls);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return shopDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ShopProductGroupDtls> getProductGroup(String userId, String shopId, String parentId) throws Exception {
		List<ShopProductGroupDtls> prdgrpDtls = null;
		String prdGrpDtlsQuery = "";
		try {
			prdGrpDtlsQuery = "from ShopProductGroupDtls grp where grp.status='A' and grp.activeYN='Y' "
					+ "and grp.userId='" + userId + "' " + "and grp.shopId='" + shopId + "' ";
			if (parentId != null && parentId != "" && !("null").equalsIgnoreCase(parentId)) {
				prdGrpDtlsQuery += " and grp.parentGrpId = " + parentId;
			}
			prdgrpDtls = sessionFac.getCurrentSession().createQuery(prdGrpDtlsQuery).list();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return prdgrpDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ShopProductDtls> getProductDtls(String userId, String shopId, String groupId) throws Exception {
		List<ShopProductDtls> prdDtls = null;
		String prdGrpDtlsQuery = "";
		try {
			prdGrpDtlsQuery = "from ShopProductDtls prd where prd.status='A' and prd.activeYN='Y' " + "and prd.userId='"
					+ userId + "' " + "and prd.shopId='" + shopId + "' ";
			if (groupId != null && groupId != "" && !("null").equalsIgnoreCase(groupId)) {
				prdGrpDtlsQuery += " and prd.shpGrpId = " + groupId;
			}
			prdDtls = sessionFac.getCurrentSession().createQuery(prdGrpDtlsQuery).list();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return prdDtls;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	public List<MetaProductGroupDtls> getDefaultProductGroup() throws Exception {
		List<MetaProductGroupDtls> metaPrdGrpDtls = null;
		String metaPrdGrpQuery = "";
		try {
			metaPrdGrpQuery = "from MetaProductGroupDtls metaPrdGrp where metaPrdGrp.status='A' "
					+ "and metaPrdGrp.activeYN ='Y'";

			metaPrdGrpDtls = sessionFac.getCurrentSession().createQuery(metaPrdGrpQuery).list();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return metaPrdGrpDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<MetaProductDtls> getDefaultProductDtls() throws Exception {
		List<MetaProductDtls> metaPrdDtls = null;
		String metaPrdDtlsQuery = "";
		try {
			metaPrdDtlsQuery = "from MetaProductDtls metaPrddtls where metaPrddtls.status='A' "
					+ "and metaPrddtls.activeYN ='Y'";

			metaPrdDtls = sessionFac.getCurrentSession().createQuery(metaPrdDtlsQuery).list();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return metaPrdDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProdVariantDtls> getProductVariants(BigDecimal productId) throws Exception {
		List<ProdVariantDtls> prodVartDtls = null;
		String prodVartQuery = "";
		try {
			prodVartQuery = "from ProdVariantDtls prodvar where prodvar.status='A' "
					+ "and prodvar.activeYN ='Y' and prodvar.productId=" + productId;

			prodVartDtls = sessionFac.getCurrentSession().createQuery(prodVartQuery).list();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return prodVartDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProdAddonDtls> getProductAddons(BigDecimal productId) throws Exception {
		List<ProdAddonDtls> prodAddDtls = null;
		String prodAddQuery = "";
		try {
			prodAddQuery = "from ProdAddonDtls prodadd where prodadd.status='A' "
					+ "and prodadd.activeYN ='Y' and prodadd.productId=" + productId;

			prodAddDtls = sessionFac.getCurrentSession().createQuery(prodAddQuery).list();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return prodAddDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ShopDetails> getShopList(BigDecimal userId) throws Exception {
		List<ShopDetails> shopDtls = null;
		String shopQuery = "";
		try {
			shopQuery = "from ShopDetails shp where shp.status='A' " + "and shp.activeYN ='Y' and shp.userId=" + userId;

			shopDtls = sessionFac.getCurrentSession().createQuery(shopQuery).list();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return shopDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ShopDetails updateShopDtls(ShopDetails shopDtls) throws Exception {
		try {
			shopDtls = (ShopDetails) sessionFac.getCurrentSession().merge(shopDtls);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return shopDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ShopProductGroupDtls createOrUpdatePrdGroup(ShopProductGroupDtls prdGrpDtls) throws Exception {
		try {
			if (prdGrpDtls.getShopGrpId() == null || "".equalsIgnoreCase(prdGrpDtls.getShopGrpId().toString())) {
				prdGrpDtls.setShopGrpId(null);
			}
			prdGrpDtls = (ShopProductGroupDtls) sessionFac.getCurrentSession().merge(prdGrpDtls);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return prdGrpDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ShopProductDtls createOrUpdateProduct(ShopProductDtls prdDtls) throws Exception {
		try {
			if (prdDtls.getShopPrdtId() == null || "".equalsIgnoreCase(prdDtls.getShopPrdtId().toString())) {
				prdDtls.setShopPrdtId(null);
			}
			prdDtls = (ShopProductDtls) sessionFac.getCurrentSession().merge(prdDtls);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return prdDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean deleteProduct(BigDecimal prodId) throws Exception {
		int count = 0;
		try {
			if (prodId.longValue() > 0) {
				count = sessionFac.getCurrentSession()
						.createNativeQuery("update cvv_usr_shp_prdt_dtls set status='D' where shp_prdt_id=" + prodId)
						.executeUpdate();
			}
			if (count > 0) {
				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return false;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createDefaultMenu(BigDecimal userId, BigDecimal shopId, BigDecimal businessId) throws Exception {
		try {
			sessionFac.getCurrentSession().createNativeQuery("select COPYDEFAULTMENU(?,?,?) from dual ")
					.setParameter(1, userId).setParameter(2, shopId).setParameter(3, businessId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<MetaVariantDtls> getMetaVariants(BigDecimal userId) throws Exception {
		List<MetaVariantDtls> variantDtls = null;
		String vartQuery = "";
		try {
			vartQuery = "from MetaVariantDtls vart where vart.status='A' "
					+ "and vart.activeYN ='Y' and (vart.userId is null or vart.userId=" + userId + ")";

			variantDtls = sessionFac.getCurrentSession().createQuery(vartQuery).list();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return variantDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<MetaAddonDtls> getMetaAddons(BigDecimal userId) throws Exception {
		List<MetaAddonDtls> addonDtls = null;
		String addonQuery = "";
		try {
			addonQuery = "from MetaAddonDtls addon where addon.status='A' "
					+ "and addon.activeYN ='Y' and (addon.userId is null or addon.userId=" + userId + ")";

			addonDtls = sessionFac.getCurrentSession().createQuery(addonQuery).list();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return addonDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean deleteMetaVariants(BigDecimal variantId) throws Exception {
		int count = 0;
		try {
			if (variantId.longValue() > 0) {
				count = sessionFac.getCurrentSession()
						.createNativeQuery("update cvv_meta_variant_dtls set status='D' where variant_id=" + variantId)
						.executeUpdate();
			}
			if (count > 0) {
				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return false;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean deleteMetaAddons(BigDecimal addonId) throws Exception {
		int count = 0;
		try {
			if (addonId.longValue() > 0) {
				count = sessionFac.getCurrentSession()
						.createNativeQuery("update cvv_meta_addon_dtls set status='D' where addon_id=" + addonId)
						.executeUpdate();
			}
			if (count > 0) {
				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return false;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public MetaVariantDtls createOrUpdateVariant(MetaVariantDtls varntDtls) throws Exception {
		try {
			if (varntDtls.getVariantId() == null || "".equalsIgnoreCase(varntDtls.getVariantId().toString())) {
				varntDtls.setVariantId(null);
			}
			varntDtls = (MetaVariantDtls) sessionFac.getCurrentSession().merge(varntDtls);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return varntDtls;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public MetaAddonDtls createOrUpdateAddon(MetaAddonDtls addonDtls) throws Exception {
		try {
			if (addonDtls.getAddonId() == null || "".equalsIgnoreCase(addonDtls.getAddonId().toString())) {
				addonDtls.setAddonId(null);
			}
			addonDtls = (MetaAddonDtls) sessionFac.getCurrentSession().merge(addonDtls);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return addonDtls;
	}

	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean checkBarcode(String barcode) throws Exception {
		try {
			List<BarcodeDtls> barcodeList = sessionFac.getCurrentSession()
					.createNativeQuery("from BarcodeDtls barcode where barcode.barcodeNumber=" + barcode)
					.getResultList();
			if (barcodeList.size() > 0) {
				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return false;
	}
	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public BarcodeDtls getBarcodeDtls(String barcode) throws Exception {
		try {
			List<BarcodeDtls> barcodeList = sessionFac.getCurrentSession()
					.createNativeQuery("from BarcodeDtls barcode where barcode.barcodeNumber=" + barcode)
					.getResultList();
			if (barcodeList.size() > 0) {
				return barcodeList.get(0);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return null;
	}
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public BarcodeDtls createBarcode(BarcodeDtls barcodeDtls) throws Exception {
		try {			
			barcodeDtls = (BarcodeDtls) sessionFac.getCurrentSession().merge(barcodeDtls);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return barcodeDtls;
	}
}