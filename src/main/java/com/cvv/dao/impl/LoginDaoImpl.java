package com.cvv.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cvv.dao.LoginDao;
import com.cvv.models.BusinessTypes;
import com.cvv.models.UserDetails;
@Repository
public class LoginDaoImpl implements LoginDao{
	
	@Autowired
	public SessionFactory sessionFac;
	
	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public boolean checkProfile(UserDetails usrDtls) throws Exception{
		List<UserDetails> usrList = null;
		try {
			usrList= sessionFac.getCurrentSession().createQuery("from UserDetails usr where usr.usrEmailId='"+usrDtls.getUsrEmailId()+"'").list();
		}
		catch(Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		if(usrList.size()>0)
			return true;
		else
			return false;
	}
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public UserDetails createProfile(UserDetails usrDtls) throws Exception{
		try {
			usrDtls = (UserDetails)sessionFac.getCurrentSession().merge(usrDtls);
		}
		catch(Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return usrDtls;
	}
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public UserDetails getProfile(String email) throws Exception{
		UserDetails usrObj = null;
		try {
			usrObj= (UserDetails)sessionFac.getCurrentSession().createQuery("from UserDetails usr where usr.usrEmailId='"+email+"'").getSingleResult();
		}
		catch(Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return usrObj;
	}
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public List<BusinessTypes> getBusinessTypeDtls() throws Exception{
		List<BusinessTypes> busList = null;
		try {
			busList= (List<BusinessTypes>)sessionFac.getCurrentSession().createQuery("from BusinessTypes bus where bus.status='A' and bus.activeYN='Y'").getResultList();
		}
		catch(Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return busList;
	}

}
