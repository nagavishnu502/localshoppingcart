package com.cvv.dao;

import java.math.BigDecimal;
import java.util.List;

import com.cvv.models.BarcodeDtls;
import com.cvv.models.MetaAddonDtls;
import com.cvv.models.MetaProductDtls;
import com.cvv.models.MetaProductGroupDtls;
import com.cvv.models.MetaVariantDtls;
import com.cvv.models.ProdAddonDtls;
import com.cvv.models.ProdVariantDtls;
import com.cvv.models.ShopDetails;
import com.cvv.models.ShopProductDtls;
import com.cvv.models.ShopProductGroupDtls;

public interface MerchantDao {
	
	public ShopDetails createShop(ShopDetails shopDtls) throws Exception;
	//public List<ShopProductGroupDtls> getProductGroup(ShopProductGroupDtls shopPrdGrpDtls) throws Exception;
	public List<ShopProductGroupDtls> getProductGroup(String userId,String shopId,String parentId) throws Exception;
	public List<ShopProductDtls> getProductDtls(String userId,String shopId,String groupId) throws Exception;
	public List<MetaProductGroupDtls> getDefaultProductGroup() throws Exception;
	public List<MetaProductDtls> getDefaultProductDtls() throws Exception;
	public List<ProdVariantDtls> getProductVariants(BigDecimal productId) throws Exception;
	public List<ProdAddonDtls> getProductAddons(BigDecimal productId) throws Exception;
	public List<ShopDetails> getShopList(BigDecimal userId) throws Exception;
	public ShopDetails updateShopDtls(ShopDetails shopDtls) throws Exception;
	public void createDefaultMenu(BigDecimal userId,BigDecimal shopId,BigDecimal businessId) throws Exception;
	public ShopProductGroupDtls createOrUpdatePrdGroup(ShopProductGroupDtls prdGrpDtls) throws Exception;
	public ShopProductDtls createOrUpdateProduct(ShopProductDtls prdDtls) throws Exception;
	public boolean deleteProduct(BigDecimal prodId) throws Exception;
	public List<MetaVariantDtls> getMetaVariants(BigDecimal userId) throws Exception;
	public List<MetaAddonDtls> getMetaAddons(BigDecimal userId) throws Exception;
	public boolean deleteMetaVariants(BigDecimal variantId) throws Exception;
	public boolean deleteMetaAddons(BigDecimal addonId) throws Exception;
	public MetaVariantDtls createOrUpdateVariant(MetaVariantDtls varntDtls) throws Exception;
	public MetaAddonDtls createOrUpdateAddon(MetaAddonDtls addonDtls) throws Exception;	
	public boolean checkBarcode(String barcode) throws Exception;
	public BarcodeDtls createBarcode(BarcodeDtls barcodeDtls) throws Exception;
	public BarcodeDtls getBarcodeDtls(String barcode) throws Exception;

}
