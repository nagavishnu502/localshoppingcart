package com.cvv.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cvv.dao.LoginDao;
import com.cvv.models.BusinessTypes;
import com.cvv.models.UserDetails;

@Service
public class LoginService {
	
	@Autowired
	public LoginDao loginDao;
	
	public boolean checkProfile(UserDetails usrDtls)throws Exception {
		return loginDao.checkProfile(usrDtls);
	}
	
	public UserDetails createProfile(UserDetails usrDtls) throws Exception{
		return loginDao.createProfile(usrDtls);
	}
	public UserDetails getProfile(String email) throws Exception{
		return loginDao.getProfile(email);
	}
	public List<BusinessTypes> getBusinessTypeDtls() throws Exception{
		return loginDao.getBusinessTypeDtls();
	}
	
}
