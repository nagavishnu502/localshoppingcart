package com.cvv.service;




import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if (username !=null) {
			UserBuilder builder = User.withUsername(username);
			builder.password("$2a$04$Q95EK6GQ7J1ITtwXQpBINOpXXTpJ6PD4fw0B2t/aSZmP9jw/uzcGO");
			builder.roles("USER","ADMIN");
			return builder.build();
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}

}