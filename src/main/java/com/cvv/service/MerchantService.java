package com.cvv.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cvv.dao.MerchantDao;
import com.cvv.models.BarcodeDtls;
import com.cvv.models.MetaAddonDtls;
import com.cvv.models.MetaProductDtls;
import com.cvv.models.MetaProductGroupDtls;
import com.cvv.models.MetaVariantDtls;
import com.cvv.models.ProdAddonDtls;
import com.cvv.models.ProdVariantDtls;
import com.cvv.models.ShopDetails;
import com.cvv.models.ShopProductDtls;
import com.cvv.models.ShopProductGroupDtls;

@Service
public class MerchantService {
	
	@Autowired
	private MerchantDao merchantDao;
	
	public ShopDetails createShop(ShopDetails shopDtls) throws Exception{
		return merchantDao.createShop(shopDtls);
	}
	
	/*public List<ShopProductGroupDtls> getProductGroup(ShopProductGroupDtls shopPrdGrpDtls) throws Exception{
		return merchantDao.getProductGroup(shopPrdGrpDtls);
	}*/
	public List<ShopProductGroupDtls> getProductGroup(String userId,String shopId,String parentId) throws Exception{
		return merchantDao.getProductGroup(userId,shopId,parentId);
	}
	public List<ShopProductDtls> getProductDtls(String userId,String shopId,String groupId) throws Exception{
		return merchantDao.getProductDtls(userId,shopId,groupId);
	}
	public List<MetaProductGroupDtls> getDefaultProductGroup() throws Exception{
		return merchantDao.getDefaultProductGroup();
	}
	public List<MetaProductDtls> getDefaultProductDtls() throws Exception{
		return merchantDao.getDefaultProductDtls();
	}
	public List<ProdVariantDtls> getProductVariants(BigDecimal productId) throws Exception{
		return merchantDao.getProductVariants(productId);
	}
	public List<ProdAddonDtls> getProductAddons(BigDecimal productId) throws Exception{
		return merchantDao.getProductAddons(productId);
	}
	public List<ShopDetails> getShopList(BigDecimal userId) throws Exception{
		return merchantDao.getShopList(userId);
	}
	public ShopDetails updateShopDtls(ShopDetails shopDtls) throws Exception{
		return merchantDao.updateShopDtls(shopDtls);
	}
	public void createDefaultMenu(BigDecimal userId,BigDecimal shopId,BigDecimal businessId) throws Exception{
		 merchantDao.createDefaultMenu(userId,shopId,businessId);
	}
	public ShopProductGroupDtls createOrUpdatePrdGroup(ShopProductGroupDtls prdGrpDtls) throws Exception{
		return merchantDao.createOrUpdatePrdGroup(prdGrpDtls);
	}
	public ShopProductDtls createOrUpdateProduct(ShopProductDtls prdDtls) throws Exception{
		return merchantDao.createOrUpdateProduct(prdDtls);
	}
	public boolean deleteProduct(BigDecimal prodId) throws Exception{
		return merchantDao.deleteProduct(prodId);
	}
	public List<MetaVariantDtls> getMetaVariants(BigDecimal userId) throws Exception{
		return merchantDao.getMetaVariants(userId);
	}
	public boolean deleteMetaVariants(BigDecimal variantId) throws Exception{
		return merchantDao.deleteMetaVariants(variantId);
	}
	public List<MetaAddonDtls> getMetaAddons(BigDecimal userId) throws Exception{
		return merchantDao.getMetaAddons(userId);
	}
	public boolean deleteMetaAddons(BigDecimal addonId) throws Exception{
		return merchantDao.deleteMetaAddons(addonId);
	}
	public MetaVariantDtls createOrUpdateVariant(MetaVariantDtls varntDtls) throws Exception{
		return merchantDao.createOrUpdateVariant(varntDtls);
	}
	public MetaAddonDtls createOrUpdateAddon(MetaAddonDtls addonDtls) throws Exception{
		return merchantDao.createOrUpdateAddon(addonDtls);
	}
	public boolean checkBarcode(String barcode) throws Exception{
		return merchantDao.checkBarcode(barcode);
	}
	public BarcodeDtls createBarcode(BarcodeDtls barcodeDtls) throws Exception{
		return merchantDao.createBarcode(barcodeDtls);
	}
	public BarcodeDtls getBarcodeDtls(String barcode) throws Exception{
		return merchantDao.getBarcodeDtls(barcode);
	}
	
}
