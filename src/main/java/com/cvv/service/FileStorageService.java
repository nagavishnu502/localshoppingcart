package com.cvv.service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cvv.models.FileDB;

@Service
public class FileStorageService {

	@Autowired
	private SessionFactory sessionFactory;
	//Session session = sessionFactory.getCurrentSession();
	/*@Autowired
	private EntityManagerFactory entityManagerFactory;
	public Session session = entityManagerFactory.unwrap(SessionFactory.class).openSession();*/

	public FileDB store(MultipartFile file) throws IOException {
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		FileDB fileDB = new FileDB(fileName, file.getContentType(), file.getBytes());

		return (com.cvv.models.FileDB) sessionFactory.getCurrentSession().save(fileDB);
	}

	public FileDB getFile(String id) {
		return sessionFactory.getCurrentSession().load(FileDB.class, id);
	}

	public Stream<FileDB> getAllFiles() {
		CriteriaBuilder builder = sessionFactory.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<FileDB> criteria = builder.createQuery(FileDB.class);
		criteria.from(FileDB.class);
		return sessionFactory.getCurrentSession().createQuery(criteria).getResultList().stream();
	}
}