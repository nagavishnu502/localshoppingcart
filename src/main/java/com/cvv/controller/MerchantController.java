package com.cvv.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cvv.models.BarcodeDtls;
import com.cvv.models.MetaAddonDtls;
import com.cvv.models.MetaProductDtls;
import com.cvv.models.MetaProductGroupDtls;
import com.cvv.models.MetaVariantDtls;
import com.cvv.models.ProdAddonDtls;
import com.cvv.models.ProdVariantDtls;
import com.cvv.models.ShopDetails;
import com.cvv.models.ShopProductDtls;
import com.cvv.models.ShopProductGroupDtls;
import com.cvv.models.UserDetails;
import com.cvv.service.MerchantService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class MerchantController {

	@Autowired
	private MerchantService merchantServ;

	@RequestMapping(method = RequestMethod.POST, value = "/merchant/createShop")
	public ShopDetails createShop(@RequestBody ShopDetails shopDtls, HttpSession session) throws Exception {
		// shopDtls.setUserId(((UserDetails)session.getAttribute("userDetails")).getUsrId());
		return merchantServ.createShop(shopDtls);
	}

	/*@RequestMapping(method = RequestMethod.POST, value = "/merchant/getProductGroup")
	public List<ShopProductGroupDtls> getProductGroup(@RequestBody ShopProductGroupDtls shopPrdGrpDtls)
			throws Exception {
		return merchantServ.getProductGroup(shopPrdGrpDtls);
	}*/
	@RequestMapping(method = RequestMethod.GET, value = "/merchant/getProductGroup/{userId}/{shopId}/{parentId}")
	public List<ShopProductGroupDtls> getProductGroup(@PathVariable("userId") String userId,@PathVariable("shopId") String shopId,
			@PathVariable("parentId") String parentId)
			throws Exception {
		return merchantServ.getProductGroup(userId,shopId,parentId);
	}

	/*@RequestMapping(method = RequestMethod.POST, value = "/merchant/getProductDtls")
	public List<ShopProductDtls> getProductDtls(@RequestBody ShopProductDtls shopPrdtDtls) throws Exception {
		return merchantServ.getProductDtls(shopPrdtDtls);
	}*/
	@RequestMapping(method = RequestMethod.GET, value = "/merchant/getProductDtls/{userId}/{shopId}/{groupId}")
	public List<ShopProductDtls> getProductDtls(@PathVariable("userId") String userId,@PathVariable("shopId") String shopId,
			@PathVariable("groupId") String groupId) throws Exception {
		return merchantServ.getProductDtls(userId,shopId,groupId);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getDefaultProductGroup")
	public List<MetaProductGroupDtls> getDefaultProductGroup() throws Exception {
		return merchantServ.getDefaultProductGroup();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getDefaultProductDtls")
	public List<MetaProductDtls> getDefaultProductDtls() throws Exception {
		return merchantServ.getDefaultProductDtls();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getBarcodeDetails/{barcode}")
	public BarcodeDtls getBarcodeDetails(@PathVariable("barcode") String barcode) throws Exception {
		ResponseEntity<String> barResponse = null;
		JSONObject json = null;
		HttpHeaders barcodeHeader = new HttpHeaders();
		barcodeHeader.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemp = new RestTemplate();
		HttpEntity<String> barRequest = new HttpEntity<String>("", barcodeHeader);
		String barcodeUrl = "https://api.barcodelookup.com/v3/products?barcode=" + barcode
				+ "&formatted=y&key=c68fjjf9ceig2q8jc9gibcuw2mu8qt";
		BarcodeDtls barcodeDtls = null;
		try {
			if(!merchantServ.checkBarcode(barcode)) {
				barResponse = restTemp.exchange(barcodeUrl, HttpMethod.POST, barRequest, String.class);
				if (barResponse != null && barResponse.getBody() != null)
					json = new JSONObject(barResponse.getBody().toString());
				ObjectMapper om = new ObjectMapper();			
				barcodeDtls = om.readValue(json.toString(), BarcodeDtls.class);
				barcodeDtls = merchantServ.createBarcode(barcodeDtls);
			}else {
				barcodeDtls = merchantServ.getBarcodeDtls(barcode);
			}
			
		} catch (JSONException jsonEx) {
			jsonEx.printStackTrace();
			throw jsonEx;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return barcodeDtls;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/merchant/getProductVariants/{productId}")
	public List<ProdVariantDtls> getProductVariants(@PathVariable("productId") BigDecimal productId) throws Exception {
		return merchantServ.getProductVariants(productId);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/merchant/getProductAddons/{productId}")
	public List<ProdAddonDtls> getProductAddons(@PathVariable("productId") BigDecimal productId) throws Exception {
		return merchantServ.getProductAddons(productId);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/merchant/getShopList/{userId}")
	public List<ShopDetails> getShopList(@PathVariable("userId") BigDecimal userId) throws Exception {
		return merchantServ.getShopList(userId);
	}

	@RequestMapping(method = RequestMethod.POST, value = "merchant/updateShopDtls")
	public ShopDetails updateShopDtls(@RequestBody ShopDetails shopDtls) throws Exception {
		return merchantServ.updateShopDtls(shopDtls);
	}
	@RequestMapping(method = RequestMethod.GET, value = "merchant/createDefaultMenu/{userId}/{shopId}/{businessId}")
	public void createDefaultMenu(@PathVariable("userId") BigDecimal userId,@PathVariable("shopId") BigDecimal shopId,
			@PathVariable("userId") BigDecimal businessId) throws Exception {
		 merchantServ.createDefaultMenu(userId,shopId,businessId);
	}
	@RequestMapping(method = RequestMethod.POST, value = "merchant/createOrUpdatePrdGroup")
	public ShopProductGroupDtls createOrUpdatePrdGroup(@RequestBody ShopProductGroupDtls prdGrpDtls) throws Exception {
		return merchantServ.createOrUpdatePrdGroup(prdGrpDtls);
	}
	@RequestMapping(method = RequestMethod.POST, value = "merchant/createOrUpdateProduct")
	public ShopProductDtls createOrUpdateProduct(@RequestBody ShopProductDtls prdDtls) throws Exception {
		return merchantServ.createOrUpdateProduct(prdDtls);
	}
	@RequestMapping(method = RequestMethod.GET, value = "merchant/deleteProduct/{productId}")
	public boolean deleteProduct(@PathVariable("productId") BigDecimal prodId) throws Exception {
		return merchantServ.deleteProduct(prodId);
	}
	@RequestMapping(method = RequestMethod.GET, value = "merchant/metaVariants/{userId}")
	public List<MetaVariantDtls> getMetaVariants(@PathVariable("userId") BigDecimal userId) throws Exception {
		return merchantServ.getMetaVariants(userId);
	}
	@RequestMapping(method = RequestMethod.GET, value = "merchant/deleteMetaVariants/{variantId}")
	public boolean deleteMetaVariants(@PathVariable("variantId") BigDecimal variantId) throws Exception {
		return merchantServ.deleteMetaVariants(variantId);
	}
	@RequestMapping(method = RequestMethod.GET, value = "merchant/metaAddons/{userId}")
	public List<MetaAddonDtls> getMetaAddons(@PathVariable("userId") BigDecimal userId) throws Exception {
		return merchantServ.getMetaAddons(userId);
	}
	@RequestMapping(method = RequestMethod.GET, value = "merchant/deleteMetaAddons/{addonId}")
	public boolean deleteMetaAddon(@PathVariable("variantId") BigDecimal addonId) throws Exception {
		return merchantServ.deleteMetaAddons(addonId);
	}
	@RequestMapping(method = RequestMethod.POST, value = "merchant/createOrUpdateVariant")
	public MetaVariantDtls createOrUpdateVariant(@RequestBody MetaVariantDtls varntDtls) throws Exception {
		return merchantServ.createOrUpdateVariant(varntDtls);
	}
	@RequestMapping(method = RequestMethod.POST, value = "merchant/createOrUpdateAddon")
	public MetaAddonDtls createOrUpdateAddon(@RequestBody MetaAddonDtls addonDtls) throws Exception {
		return merchantServ.createOrUpdateAddon(addonDtls);
	}
}
