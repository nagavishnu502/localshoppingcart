package com.cvv.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cvv.models.JwtRequest;
import com.cvv.models.JwtResponse;
import com.cvv.security.JwtTokenUtil;
import com.cvv.util.OAuthUtils;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.model.Userinfo;

import io.jsonwebtoken.impl.DefaultClaims;

@RestController
public class LoginController {

	static final String CLIENT_ID = "231887092353-vql4ifdefjrfjh12raoldac3849rogdq.apps.googleusercontent.com";
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	
	@RequestMapping(path = "/welcomeNote", method = RequestMethod.GET)
	public String welcomeNote() throws Exception {
		return "Welcome Vishnu!";
	}

	@RequestMapping(path = "/loginAuth/{emailId}", method = RequestMethod.POST)
	public Userinfo loginGmailAuth(@PathVariable("emailId") String emailId, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		Userinfo userInfo = null;
		try {
			OAuthUtils.newFlow();
			String sessionId = request.getSession().getId();
			boolean isUserLoggedIn = OAuthUtils.isUserLoggedIn(sessionId);

			if (isUserLoggedIn)
				userInfo = OAuthUtils.getUserInfo(sessionId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return userInfo;
	}

	@RequestMapping(path = "/loginTokenAuth", method = RequestMethod.POST)
	public Userinfo loginGmailTokenAuth(@RequestParam("emailId") String emailId,
			@RequestParam("tokenId") String tokenId,HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		Userinfo userInfo = new Userinfo();
		try {
			GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(HTTP_TRANSPORT,
					JacksonFactory.getDefaultInstance())
							// Specify the CLIENT_ID of the app that accesses the backend:
							.setAudience(Collections.singletonList(CLIENT_ID))
							// Or, if multiple clients access the backend:
							// .setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
							.build();	
			authenticate(emailId, "secret");
			final String token = jwtTokenUtil.generateToken(emailId);
			response.setHeader("token", token);
			/*GoogleIdToken idToken = verifier.verify(tokenId);
			if (idToken != null) {
				Payload payload = idToken.getPayload();
				String userId = payload.getSubject();
				// Print user identifier
				if (payload.getEmail() != null && payload.getEmail().equalsIgnoreCase(emailId)) {

					System.out.println("User ID: " + userId);

					// Get profile information from payload
					userInfo.setEmail((String) payload.getEmail());
					userInfo.setVerifiedEmail(Boolean.valueOf(payload.getEmailVerified()));
					userInfo.setName((String) payload.get("name"));
					userInfo.setPicture((String) payload.get("picture"));
					userInfo.setLocale((String) payload.get("locale"));
					userInfo.setFamilyName((String) payload.get("family_name"));
					userInfo.setGivenName((String) payload.get("given_name"));
					authenticate(userInfo.getEmail(), "secret");
					final String token = jwtTokenUtil.generateToken(userInfo.getEmail());
					response.setHeader("token", token);
				}

			} else {
				System.out.println("Invalid ID token.");
				throw new Exception("Invalid ID token.");
			}*/
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}

		return userInfo;
	}
	@RequestMapping(value = "/refreshtoken", method = RequestMethod.GET)
	public void refreshtoken(HttpServletRequest request,HttpServletResponse response) throws Exception {
		// From the HttpRequest get the claims
		DefaultClaims claims = (io.jsonwebtoken.impl.DefaultClaims) request.getAttribute("claims");

		Map<String, Object> expectedMap = getMapFromIoJsonwebtokenClaims(claims);
		String token = jwtTokenUtil.doGenerateRefreshToken(expectedMap, expectedMap.get("sub").toString());
		response.setHeader("token", token);
	}

	public Map<String, Object> getMapFromIoJsonwebtokenClaims(DefaultClaims claims) {
		Map<String, Object> expectedMap = new HashMap<String, Object>();
		for (Entry<String, Object> entry : claims.entrySet()) {
			expectedMap.put(entry.getKey(), entry.getValue());
		}
		return expectedMap;
	}

	private void authenticate(String username, String password) throws Exception {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}


}