package com.cvv.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cvv.models.BusinessTypes;
import com.cvv.models.ShopDetails;
import com.cvv.models.UserDetails;
import com.cvv.service.LoginService;

@RestController
public class UserController {
	
	private UserDetails merchantProfile;
	
	@Autowired
	public LoginService loginserv;
	@RequestMapping(path = "/createProfile", method = RequestMethod.POST)
	public UserDetails createProfile(@RequestBody UserDetails usrDtls,HttpSession session) throws Exception {
		UserDetails usrRtnDtls = loginserv.createProfile(usrDtls);
		session.setAttribute("userDetails", usrRtnDtls);
		return usrRtnDtls;
	}
	
	@RequestMapping(path = "/checkProfile", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	public JSONObject checkProfile(@RequestBody UserDetails usrDtls) throws Exception {
		return  new JSONObject("{'Status':"+loginserv.checkProfile(usrDtls)+"}");
	}
	@RequestMapping(path="/merchant/profile/{email}", method = RequestMethod.GET)
	public UserDetails getProfile(@PathVariable("email") String email,HttpSession session) throws Exception {
		UserDetails usrRtnDtls = loginserv.getProfile(email);
		session.setAttribute("userDetails", usrRtnDtls);
		return usrRtnDtls;
	}
	@RequestMapping(path = "/getBusinessTypeDtls", method = RequestMethod.GET)
	public List<BusinessTypes> getBusinessTypeDtls() throws Exception {
		return  loginserv.getBusinessTypeDtls();
	}
	
}
